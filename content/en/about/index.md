---
title: "About"
featured_image: 'background_python_programming.jpg'
menu:
  main:
    weight: 1
---

We are a team of optical communication and integrated photonics researchers and have combined more than 25 years of laboratory and research automation experience. We also all share an interest in the Python programming language and open-source community projects. During the last few years of attending conferences, we have found that there is a multitude of free software and tools available to simplify virtually every aspect of optical communications research.

We are strongly convinced that everyone could benefit in their everyday research activities from the freely available tools, if one would just know about them. Hence, we started running the "Hack your Research!" events at the two major optical communications conferences to bring like-minded people together and offer a platform for exchange of ideas, tools, tips and tricks. The events usually consist of a few tables where invited participants give a hands-on demonstration of their tool to a small audience in an interactive format. Asking questions is strongly encouraged! Thanks to our sponsors, nobody has to go home hungry or thirsty.

The "Hack your Reserach!" events are the successor of the vastly successful [Lab Automation Hackathons](https://python4photonics.gitlab.io/) that were originally started in 2016. At the time of writing (2022), the project and event series was handed-over to a younger generation of four highly motivated newcomers. With the reboot of the series, we have decided to broaden the range of topics from originally only lab automation with Python to accomodate all the cool, free projects within our research community. The original founding team still supports the events by looking over our shoulders as the advisory committee.

If you want to join us and help organizing awesome events, please don't hesitate to reach out.

**Organizers:**  
[Amol Delmade](https://www.linkedin.com/in/amol-s-delmade/), _Dublin City University, Ireland_  
[Besma Kalla](https://www.linkedin.com/in/besma-kalla/), _Eindhoven University of Technology, Netherlands_  
[Giammarco Di Sciullo](https://www.linkedin.com/in/giammarco-di-sciullo/), _Università degli Studi dell'Aquila, Italy_  
[Henrique Buglia](https://www.linkedin.com/in/henriquebuglia/), _University College London, United Kingdom_  
[Marco Eppenberger](https://www.linkedin.com/in/marcoep/), _PsiQuantum, USA_  
[Menno van den Hout](https://www.linkedin.com/in/mennovdhout/), _Eindhoven University of Technology, Netherlands_  
[Mohamad Hossein Idjadi](https://www.linkedin.com/in/idjadi/), _Nokia Bell Labs, USA_  
[Vincent van Vliet](https://www.linkedin.com/in/vanvlietvincent/), _Eindhoven University of Technology, Netherlands_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Binbin Guan, _Microsoft, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_  


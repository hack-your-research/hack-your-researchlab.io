---
title: "Hack your Research!"
description: "Tools and Tricks for Today’s Telecommunications Techies<br />(formerly Lab Automation Hackathon)"
cascade:
  featured_image: 'background_python_programming.jpg'
---

"Hack your Research!" are recurring meetings at the [OFC](https://www.ofcconference.org/) and [ECOC](https://www.ecoc2024.org/about-ecoc/future-conferences/) scientific conferences. These events aim to provide a platform for exchanging ideas, techniques, software and tools developed by PhD students and researchers alike with a focus on free and/or open source materials.

From students to highly experienced experts, everybody is welcome to learn and share ways to boost their research productivity!

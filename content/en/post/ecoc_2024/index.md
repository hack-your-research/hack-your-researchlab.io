---
date: 2024-07-23T21:46:08-08:00
description: "Event details and list of presenters for ECOC 2024."
featured_image: "photos/IMG-20241003-WA0003.jpg"
tags: ["ECOC", "2024"]
title: "Hack your Research! @ ECOC 2024"
author: "Marco"
draft: false
---

**Event Date:** Wednesday, 25 September 2024, 6:00pm – 8:00pm CEST  

**Event Location:** Room Illusion, Congress Center Messe Frankfurt, Germany  

**Link:**
[ecoc2024.org/special-events](https://www.ecoc2024.org/special-events/lab-automation-hackathon/)

**Organizers:**  
[Besma Kalla](https://www.linkedin.com/in/besma-kalla/), _Eindhoven University of Technology, Netherlands_  
[Amol Delmade](https://www.linkedin.com/in/amol-s-delmade/), _Dublin City University, Ireland_  
[Giammarco Di Sciullo](https://www.linkedin.com/in/giammarco-di-sciullo/), _Università degli Studi dell'Aquila, Italy_  
[Menno van den Hout](https://www.linkedin.com/in/mennovdhout/), _Eindhoven University of Technology, Netherlands_  
[Vincent van Vliet](https://www.linkedin.com/in/vanvlietvincent/), _Eindhoven University of Technology, Netherlands_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_  
[Marco Eppenberger](https://www.linkedin.com/in/marcoep/), _PsiQuantum, USA_  

### Demos:

> **Automatized 3D-Printed Polarization Controller in Action**  
> by Iñaki Beraza, Technical University of Denmark, Denmark  
> _Commercially available polarization controllers are not always the desired size. Plus, automatic ones are expensive and hard to customize. Therefore, we designed and 3D-printed our own PC and automatized it. In this demo, we will show the PC and how we can control it through simple functions and GUI, using Arduino._  

> **Simulating optical turbulence and adaptive optics with FAST**  
> by Ollie Farley, Durham University, United Kingdom  
> _FAST is an open source, python-based simulation tool that allows rapid characterisation of the effect of optical turbulence on ground-space uplink/downlink, including mitigation with adaptive optics. We will explore how to set-up and run simulations, showing just how quickly you can obtain useful results._  

> **Making figures and animations using tikz**  
> by Kadir Gümüş, Eindhoven University of Technology, the Netherlands  
> _In this demo I will show how to use tikz to make good-looking figures and animations for use in presentations and papers._  

> **Lab Automation in Large-Scale Transmission Experiments**  
> by Giammarco Di Sciullo, University of L'Aquila, Italy  
> _Lab automation is crucial for enabling large-scale space-division multiplexed transmission experiments. Let’s explore the experimental setups used in recent high-capacity trials and discuss how automation can simplify and enhance the measurement processes._  

> **Rapid prototyping - getting started with CAD and 3D printing tools**  
> by Lauren Dallachiesa, Nokia Bell Labs, USA  
> _Let's chat about some 3D modeling basics like getting started with Fusion 360, discuss what free CAD tools exist out there, as well as how to use affordable 3D printers to design and fabricate prototypes faster and easier!_  

> **Innovative Optoelectronic Testing and Control Solutions**  
> by Gaurav Jain, Pilot Photonics, Ireland  
> _In-house electronics enabling fast prototyping. In this years Hack You Research Pilot Photonics will be showcasing it's in-house electronics and associated interfaces required for very efficient testing and control of optoelectronic devices. Pilot Photonics will also be showcasing it's easy to use software which can be very efficiently customized for the characterization and operation of multitude of optoelectronic devices._  

> **LabVIEW and MATLAB the Dream Team for the Optical Transmission Lab?**  
> by Robert Emmerich, Fraunhofer HHI, Germany  
> _The advent of open source has led to a decline in the use of proprietary software environments, such as the graphical programming language LabVIEW and MATLAB. In this demonstration, we will present experiments from our Berlin laboratory using our MATLAB-based coherent transport DSP in conjunction with a LabVIEW-based GUI for configuration and DSP control._  

> **Software-defined photonics for datacenter applications**  
> by David Sánchez, iPronics, Spain  
> _During this demonstration, we will show the first prototype of the iPronics Optical Networking Engine for datacenter management applications that uses Python. The demonstration will include different switching strategy implementations, processing and loss-less operation._  

> **Revolutionize Chip Design with Python**  
> by Joaquin Matres, GDSFactory, USA  
> _For efficient design, verification and validation of integrated circuits and components it is important to have an easy to customize and extend workflow. Python has become the standard programming language for machine learning, scientific computing and engineering. In this tutorial we will use machine learning tools (python, jupyter notebooks, and generative AI) to design 3 types of chips. A heater, a photonic filter and a ring resonator. This will allow you to leverage the machine learning tools into chip design (GDSFactory, Femwell and SAX)._  

> **Empowering Photonics Design: Macro-based functionalities and Python-based Machine Learning Simulations**  
> by Elias Giacoumidis and Shi Li, VPI photonics, Germany  
> _Join VPIphotonics experts to explore Python-based techniques and Macros for photonics simulations. Discover the updated VPItoolkit Machine Learning Framework with new applications, supervised and unsupervised algorithms, and integration with JupyterLab and TensorBoard, along with automation of schematic creation and improved toolkit interoperability._  


### Lightning Talks:

> **Global Optimisation in Matlab vs Python**  
> by Vincent Wüst, Karlsruhe Institute of Technology, Germany  
> _Pros and cons for using Matlab vs Python for global optimisation problems._  

> **IN RUST WE TRUST!**  
> by Giovanni Simone Sticca, Politecnico di Milano, Italy  
> _Optical network designers struggle between hard-to-use low-level programming languages and slow high-level ones. Rust offers an efficient, user-friendly solution for complex designs._


### Photos:

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0003.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0007.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0015.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0035.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0037.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0039.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0043.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0045.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0051.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0053.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0059.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0067.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20240927-WA0069.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20241003-WA0002.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20241003-WA0003.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20241003-WA0012.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20241003-WA0028.jpg)

![Photo of Hack Your Research at ECOC 2024](photos/IMG-20241003-WA0030.jpg)


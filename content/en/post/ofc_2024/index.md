---
date: 2024-01-10T10:58:08-04:00
description: "Event details for Hack Your Research! at OFC 2024."
featured_image: "photos/point-monitor.jpg"
tags: ["OFC", "2024"]
title: "Hack your Research! @ OFC 2024"
author: "Vincent"
draft: false
---

**Event Date:** Sunday, 24 March 2024, 7pm – 9pm PST  

**Event Location:** Room 6A, San Diego Convention Center, San Diego, California, USA  

**Link:**
[ofcconference.org/special-events](https://www.ofcconference.org/en-us/home/program-speakers/special-events/hackathon/)

**Organizers:**  
Henrique Buglia, _University College London, United Kingdom_  
Marco Eppenberger, _PsiQuantum, USA_  
Menno van den Hout, _Eindhoven University of Technology, Netherlands_  
Vincent van Vliet, _Eindhoven University of Technology, Netherlands_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Binbin Guan, _Microsoft, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_ 

### Demos:

> **Julia: Your new secret romance?**  
> by Jonas Krimmer, Karlsruhe Institute of Technology, Germany  
> _Julia is an open-source programming language enabling C-level performance with a syntax similar to MATLAB or Python. In this demonstration, you will learn how Julia can speed up your algorithms, simulations, and data processing chains while being interoperable with existing Python or C libraries._  

> **Fine-Tuning Your DSP with Machine Learning**  
> by Manuel S. Neves, Instituto de Telecomunicações, Universidade de Aveiro, Portugal  
> _Join in for a live demo featuring a machine learning tool built with Python's TensorFlow library. Learn how to optimize Digital Signal Processing (DSP) subsystems while considering real-world constraints like bandwidth, complexity, and transceiver nonlinearities. See how machine learning streamlines DSP design for performance._  

> **LabExT the all in one Lab Automation Tool**  
> by Laurenz Kulmer, ETH Zurich, Switzerland  
> _We are presenting a Lab automation tool developt at our institute with some new features integrated which can greatly improve your daily workflow._  

> **Making figures and animations using tikz**  
> by Kadir Gümüş, Eindhoven University of Technology, Netherlands  
> _In this demo I will show how to use tikz to make good-looking figures and animations for use in presentations and papers._  

> **Space-Division-Multiplexed Transmission Setups and Field Trials**  
> by Giammarco Di Sciullo, University of L'Aquila, Italy  
> _Space-division multiplexed transmission experiments in multi-core and multi-mode fibers, focusing on the experimental setups used in recent high-capacity experiments, are discussed. In addition, the first field-deployed SDM testbed located in the city of L’Aquila is presented._  

> **Physical Layer Simulation and Optimization with MOKka**  
> by Andrej Rode, Vincent Lauinger, Shrinivas Chimmalgi, and Laurent Schmalen, Karlsruhe Institute of Technology, Germany  
> _Machine learning for optimization of the physical layer is currently a popular research topic. To aid research in this field, we introduce our Python library MOKka. We show an interactive demo including constellation shaping, optical channel models and various signal processing blocks._  

> **XLRON: Accelerated Reinforcement Learning for Optical Networks**  
> by Michael Doherty, University College London, UK  
> _XLRON is an open source package built with the JAX numerical computing framework that allows optical network resource allocation problems to be simulated on GPU, giving 2-3 orders of magnitude speed up over CPU-dependent approaches._  

> **Rapid Prototyping: 3D Printing and Modelling**  
> by Lauren Dallachiesa, Nokia Bell Labs, USA  
> _Let's chat about some 3D modeling basics like getting started with Fusion 360, discuss what free CAD tools exist out there, as well as how to use affordable 3D printers to design and fabricate prototypes faster and easier!_  

> **LabVIEW for the Optical Transmission Lab: Outdated or Underestimated ?**  
> by Robert Emmerich, Carsten Schmidt-Langhorst, and Robert Elschner, Fraunhofer HHI, Germany  
> _With the rise of open source, the use of the proprietary graphical programming environment Labview has declined. In this demo, we will present a live experiment using our Matlab-based coherent transport DSP in conjunction with a Labview-based GUI for configuration and lab automation._  

> **Software-defined photonics for datacenter applications**  
> by Daniel Pérez-López, Tsjerk Hoekstra, and David Sánchez, iPronics, Spain  
> _During this demonstration, we will show the first prototype of the iPronics Optical Networking Engine for datacenter management applications that uses Python. The demonstration will include different switching strategy implementations, processing and loss-less operation._  

> **Revolutionize Your Chip Design with GDSfactory**  
> by Joaquin Matres, Google X, USA, and Floris Laporte, Rockley Photonics, USA  
> _In this tutorial we will use machine learning tools (python, jupyter notebooks, and generative AI) to design 3 types of chips. A heater, an MZI filter and a ring resonator. This will allow you to leverage the machine learning tools into chip design._  

> **Consistent PIC simulation and layout design by Ansys Lumerical and open-source layout tools interoperability**  
> by Parya Samadian, Ansys, Canada  
> _During an iterative integrated photonics design process, maintaining the consistency between component/circuit layout design and simulation is challenging. Here we demonstrate Ansys Lumerical's interoperability with open-source layout tools to streamline this process._  

> **Advanced Photonic Design Automation: ML-Based Equalization and Automated Components Creation**  
> by Elias Giacoumidis and Eugene Sokalau, VPI photonics, Germany  
> _Join us for live demonstrations of machine learning-based equalization in optical transmission systems and device characterization using VPIphotonics ML Framework, as well as the automated creation of photonic components based on the measured S-matrix._  


### Lightning Talks:

> **femwell**  
> by Nicolas Fontaine, Nokia Bell Labs, USA  
> _Finite elements in python? FEMWELL_  
> see https://github.com/HelgeGehring/femwell  

> **xarray: store all the data!**  
> by Marijn Rombouts, Eindhoven University of Technology, Netherlands  
> _Managing multidimensional measurement or simulation data can be a challenge in Python, but not with xarray!_  
> see https://github.com/pydata/xarray

> **Automating experiments in Python**  
> by Carlos Rubio García, Eindhoven University of Technology, Netherlands  
> _Python tool for automating and controlling experiment with remote equipment_  

> **linien: a laser stabilization app for the Red Pitaya FPGA**  
> by Andrei Isichenko, UC Santa Barbara, USA  
> _Linien is a user-friendly Python-based app for the Red Pitaya FPGA for laser frequency locking that helps get experiments running quickly._  
> see https://github.com/linien-org/linien  

> **Revolutionize your chip design with SAX**  
> by Joaquin Matres, Google X, USA  
> _Autograd and XLA for S-parameters - a scatter parameter circuit simulator and optimizer for the frequency domain based on JAX._  
> see https://github.com/flaport/sax  

### Photos:

![Photo of Hack Your Research at OFC 2024](photos/2-men-enage.jpg)

![Photo of Hack Your Research at OFC 2024](photos/2-men-study.jpg)

![Photo of Hack Your Research at OFC 2024](photos/3-men-backdrop.jpg)

![Photo of Hack Your Research at OFC 2024](photos/3-men-casual2.jpg)

![Photo of Hack Your Research at OFC 2024](photos/4-men-engage.jpg)

![Photo of Hack Your Research at OFC 2024](photos/engrossed.jpg)

![Photo of Hack Your Research at OFC 2024](photos/laptop-stats.jpg)

![Photo of Hack Your Research at OFC 2024](photos/man_detail.jpg)

![Photo of Hack Your Research at OFC 2024](photos/man-speaks-group.jpg)

![Photo of Hack Your Research at OFC 2024](photos/monitor2.jpg)

![Photo of Hack Your Research at OFC 2024](photos/point-monitor.jpg)

![Photo of Hack Your Research at OFC 2024](photos/table-chat.jpg)

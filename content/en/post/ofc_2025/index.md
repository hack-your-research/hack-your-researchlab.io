---
date: 2025-01-03T01:23:45-08:00
description: "Event details and list of presenters for OFC 2025."
featured_image: "background_python_programming.jpg"
tags: ["OFC", "2025"]
title: "Hack your Research! @ OFC 2025"
author: "Marco"
draft: false
---

**Event Date:** Sunday, 30th March 2025, 7pm to 9pm PDT  

**Event Location:** Room 303, Moscone Center, San Francisco, California, USA

**Link:**
[ofcconference.org - Special Events](https://www.ofcconference.org/en-us/home/program-speakers/special-events/)

**Organizers:**  
[Besma Kalla](https://www.linkedin.com/in/besma-kalla/), _Eindhoven University of Technology, Netherlands_  
[Amol Delmade](https://www.linkedin.com/in/amol-s-delmade/), _Dublin City University, Ireland_  
[Giammarco Di Sciullo](https://www.linkedin.com/in/giammarco-di-sciullo/), _Università degli Studi dell'Aquila, Italy_  
[Menno van den Hout](https://www.linkedin.com/in/mennovdhout/), _Eindhoven University of Technology, Netherlands_  
[Vincent van Vliet](https://www.linkedin.com/in/vanvlietvincent/), _Eindhoven University of Technology, Netherlands_  
[Marco Eppenberger](https://www.linkedin.com/in/marcoep/), _PsiQuantum, USA_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_  
[Henrique Buglia](https://www.linkedin.com/in/henriquebuglia/), _University College London, United Kingdom_  

stay tuned for more info...


---
date: 2023-06-19T14:58:08-04:00
description: "Event details and list of presenters for ECOC 2023."
featured_image: "photos/IEEE_phot_soc_4.png"
tags: ["ECOC", "2023"]
title: "Hack your Research! @ ECOC 2023"
author: "Vincent"
draft: false
---

**Event Date:** Sunday, 01 October 2023, 6pm – 8pm BST  

**Event Location:** Room Alsh, Scottish Event Campus, Glasgow, Scotland  

**Link:**
[ecoc2023.theiet.org](https://ecoc2023.theiet.org/programme/special-events/)

**Organizers:**  
Henrique Buglia, _University College London, United Kingdom_  
Marco Eppenberger, _PsiQuantum, USA_  
Menno van den Hout, _Eindhoven University of Technology, Netherlands_  
Vincent van Vliet, _Eindhoven University of Technology, Netherlands_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Binbin Guan, _Microsoft, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_ 

### Demos:

> **Julia: Your New Secret Romance?**  
> by Jonas Krimmer, Karlsruhe Institute of Technology, Germany  
> _Julia is an open-source programming language enabling C-level performance with a syntax similar to MATLAB or Python. In this demonstration, you will learn how Julia can speed up your algorithms, simulations, and data processing chains while being interoperable with existing Python or C libraries._  
> see [the demo slides](/ecoc2023_Jonas_Krimmer_Julia_slides.pdf) and [the demo materials](/ecoc2023_Jonas_Krimmer_Julia_materials.zip)  

> **Free-Space Optics on our Days: a Real-Time Demonstration**  
> by Marco A. Fernandes and Manuel M. Freitas, Instituto de Telecomunicações, Universidade de Aveiro, Portugal  
> _Experience firsthand the benefits of free-space optics in our daily routines through the seamless transmission of high-definition videos in real-time over an FSO link._

> **ML-Based Constellation Shaping for Arbitrary Channels**  
> by Beatriz M. Oliveira and Manuel S. Neves, Instituto de Telecomunicações, Universidade de Aveiro, Portugal  
> _Join us for a tutorial on how to implement modular autoencoders in Python using end-to-end learning. You will gain knowledge about ML DSP optimization tools, specifically for the AWGN channel and unamplified coherent systems. Material will be provided to follow along the demo._

> **Space-Division-Multiplexed Transmission Setups and Field Trials**  
> by Giammarco Di Sciullo, Universita degli Studi dell'Aquila, Italy  
> _Space-division multiplexed transmission experiments in multi-core and multi-mode fibers, focusing on the experimental setups used in recent high-capacity experiments, are discussed. In addition, the first field-deployed SDM testbed located in the city of L’Aquila is presented._  
> see [the demo slides](/ecoc2023_Giammarco_DiSciullo_slides.pdf)  

> **Automation and Visualisation for Ultra-High-Count WDM/SDM Channel Measurements**  
> by Seiya Sumita, Shohei Beppu, and Yuta Wakayama, KDDI Research Inc., Japan  
> _We present how we automate and visualise WDM/SDM hero experiments in our Lab. During this demonstration, we show various examples of fibre characterisation, device calibration, channel switching, database management, and so on._

> **Quantifying the Amount of Excess Noise due to Residual Phase Noise in a CVQKD System**  
> by Hou-Man Chin, Technical University of Denmark, Denmark  
> _Determining the contributors of excess noise in a CVQKD system can be a challenging problem. Here we discuss a method that we've used to evaluate how impactful residual phase noise, due to imperfect digital compensation, is in our experimental systems with respect to excess noise._  
> see [the demo slides](/ecoc2023_Hou-Man_Chin_slides.pptx)  

> **A GPU Primer: Tips for Getting Started with GPUs for Research Applications**  
> by Domaniç Lavery (Infinera, Canada), Siddharth Varughese (Infinera, USA)  
> _GPU-accelerated processing has recently been popularised by ML applications. But could you use GPUs in your own research? Let us help you get started! We will discuss pros and cons of GPUs, common pitfalls, and show some ECOC-friendly code examples running live on an eGPU._  
> see [the demo slides](/ecoc2023_Domanic_Lavery_GPU_slides.pdf)

> **Gdsfactory, an Open Source Python Flow for Circuit Design, Verification and Validation**  
> by Lucas Heitzman, Flexcompute, USA  
> _For efficient design, verification and validation of integrated circuits and components it is important to have an easy to customize and extend workflow. Gdsfactory provides you an end to end workflow that combines layout, verification and validation using an extensible, open source, python driven flow for turning your chip designs into validated products._  
> see https://gdsfactory.github.io/

> **Prototyping in VPI DesignSuite: Exploring New Features and Enhancements**  
> by Shi Li and Elias Giacoumidis, VPIphotonics, Germany  
> _Explore the Python API support and latest advancements in VPI DesignSuite. Join our team of experts to streamline prototyping and unleash your creativity in a cutting-edge photonic design environment. Experience a seamless transition from concept to reality and gain insights into debugging techniques._  
> see https://www.VPIphotonics.com

> **Tidy3D: Large-Scale High-Speed FDTD Solver**  
> by Lucas Gabrielli and Casey Wojcik, Flexcompute, USA  
> _Tidy3D is Flexcompute's ultrafast FDTD simulation tool for large-scale optical simulations. Our cloud solver can be used from a web-based graphical interface or a Python package, facilitating design exploration, integration and automation. This demo will show the lightning-fast speed of Tidy3D and its newest features._  
> see https://www.flexcompute.com/tidy3d

> **Learn how to Reduce your Photonic Design Cycles and make your Prototypes Faster with Programmable Photonics**  
> by Daniel Perez-Lopez, David Sánchez, and Mikel Gutierrez, iPronics, Spain  
> _During this demonstration, iPronics team will show how to generate critical photonic functions in minutes using the iPronics SmartLight Processor. Implementations will include interconnects, arbitrary switching, splitters, combiners, independent phase configuration, filtering and equalization._  
> see https://ipronics.com/

> **Automated characterization of PICs: framework and standards**  
> by Sylwester Latkowski, TU/e – Photonic integration Technology Center (PITC), Eindhoven, Netherlands  
> _In this demo, we present our automated PIC measurement methodology. It covers overall system architecture, hardware automation, test definitions, and measurement data formats. We will demonstrate a complete measurement cycle using a digital twin of our setup._  

### Photos:

![Photo of Hack Your Research at ECOC 2023](photos/IEEE_phot_soc_1.png)

![Photo of Hack Your Research at ECOC 2023](photos/IMG_0040.JPEG)

![Photo of Hack Your Research at ECOC 2023](photos/IEEE_phot_soc_2.png)

![Photo of Hack Your Research at ECOC 2023](photos/IEEE_phot_soc_3.png)

![Photo of Hack Your Research at ECOC 2023](photos/IEEE_phot_soc_4.png)

![Photo of Hack Your Research at ECOC 2023](photos/IMG_0061.JPEG)

![Photo of Hack Your Research at ECOC 2023](photos/IMG_0055.JPEG)

![Photo of Hack Your Research at ECOC 2023](photos/IMG_0057.JPEG)

![Photo of Hack Your Research at ECOC 2023](photos/IMG_0038.JPEG)
---
date: 2024-07-23T21:47:08-08:00
description: "Event details and list of presenters for IPC 2024."
featured_image: "photos/IMG_5008.jpg"
tags: ["IPC", "2024"]
title: "Hack your Research! @ IPC 2024"
author: "Vincent"
draft: false
---

**Event Date:** Sunday, 10 November 2024, 4pm – 5:30pm CEST  

**Event Location:** Sala Cosmo II, Cardo Roma Autograph Collection, Rome, Italy  

**Link:**
[ieee-ipc.org/sunday-program/](https://ieee-ipc.org/program/sunday-program/)

**Organizers:**  
[Giammarco Di Sciullo](https://www.linkedin.com/in/giammarco-di-sciullo/), _Università degli Studi dell'Aquila, Italy_  
[Amol Delmade](https://www.linkedin.com/in/amol-s-delmade/), _Dublin City University, Ireland_  
[Besma Kalla](https://www.linkedin.com/in/besma-kalla/), _Eindhoven University of Technology, Netherlands_  
[Mohamad Hossein Idjadi](https://www.linkedin.com/in/idjadi/), _Nokia Bell Labs, USA_  
[Vincent van Vliet](https://www.linkedin.com/in/vanvlietvincent/), _Eindhoven University of Technology, Netherlands_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_  
[Marco Eppenberger](https://www.linkedin.com/in/marcoep/), _PsiQuantum, USA_  
[Menno van den Hout](https://www.linkedin.com/in/mennovdhout/), _Eindhoven University of Technology, Netherlands_  

## Demos:

> **Speeding up your MATLAB Code to the MEX!**  
> by Kadir Gümüş, Eindhoven University of Technology, the Netherlands  
> _Are you tired of having to wait for your simulations to finish? Stressed that you will miss a deadline because your experimental data still needs to be processed? In this talk I will give a short introduction on how to use mex functions in MATLAB to speed up your code. Mex functions are C/C++ functions which can be called as standard MATLAB functions. I will also go into mexcuda functions, which are mex functions which can leverage the GPU through cuda for speeding up your code._  

> **Energy-Aware Optical Networks and Services**  
> by Carlo Centofanti, University of L'Aquila, Italy  
> _Presenting real-time energy monitoring for optical networks and services to boost sustainability through precise observability and optimized power management._  

> **Automatized 3D-Printed Polarization Controller in Action**  
> by Iñaki Beraza, Technical University of Denmark, Denmark  
> _Comercially available Polarization Controllers (PC) are not always the desired size. Plus, automatized ones are expensive and hard to customize. Therefore, we designed and 3D printed our own PCs to control modes, connected it to some stepper motors and aurduino and wrote a python script to manage it._  

> **Symbiotic Constellations: Digital Coherent Optics Onboard Satellite Networks**  
> by Igor P. Vieira and Darli A. A. Mello, Department of Communications, University of Campinas, Brazil  
> _In this demo, we present a Matlab/Octave-based interface developed to integrate satellite orbital dynamics — in ultra-dense low-Earth orbit (LEO) constellations — with optical system performance analysis. By tracking the evolution of communication links over time, whether in inter-satellite or satellite-to-ground/ground-to-satellite scenario, the tool provides estimates of key parameters for the design and operation of satellite optical communication systems, including coverage profile, line-of-sight/access intervals, Doppler shift, free-space path loss (FSPL), SNR, etc. Our simulator comprises animated visualizations that display the changes experienced by these variables along the satellites' orbital path._  

> **Lab Automation in Large-Scale Transmission Experiments**  
> by Giammarco Di Sciullo, University of L'Aquila, Italy  
> _Lab automation is crucial for enabling large-scale space-division multiplexed transmission experiments. Let’s explore the experimental setups used in recent high-capacity trials and discuss how automation can simplify and enhance the measurement processes._  

> **Introducing GNPy as Open-Source Dataset Generation for Machine Learning in Optical Networks**  
> by Francesco Aquilino, LINKS Foundation, Italy, and Renato Ambrosone, Politecnico di Torino, Italy  
> _This presentation introduces GNPy as an open-source tool for simulating optical network performance, showcasing a use case where GNPy is used to generate datasets for machine learning model training. We will explore the benefits of leveraging GNPy to create realistic data for transmission quality prediction and network optimization._  

> **Red Pitaya – an inexpensive all-in-one lab instrument**  
> by Mohamad Idjadi, Nokia Bell Labs, USA  
> _Laboratory instruments can be expensive. Researchers often need to perform simple tasks, such as monitoring low-speed signals with an oscilloscope, synthesizing RF signals, or controlling systems with a PID controller. Acquiring dedicated, application-specific instruments, like high-speed multi-channel oscilloscopes or low-noise PID controllers, can be cost-prohibitive and impractical. Advanced FPGA solutions offer a more affordable and versatile alternative, capable of performing various hardware functions. This demonstration introduces Red Pitaya FPGA as a powerful, cost-effective, and efficient tool for researchers and students._  

> **Custom Dither-free Bias Controllers for Multi-Modulator Setups**  
> by Sreeraj S, Indian Institute of Technology Madras, India  
> _We demonstrate an in-house built dither-free bias controller that can bias multiple modulators at arbitrary bias points with reduced hardware. We use a single high-resolution optical power sensing system with 16-bit resolution at 8 different current levels, along with an electronic multiplexing scheme to control the bias of multiple modulators. This controller can be used for multiple modulators or dual parallel modulators and is more suited to microwave photonic applications._  


### Photos:

![Photo of Hack Your Research at IPC 2024](photos/IMG_5008.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05634.jpg)

![Photo of Hack Your Research at IPC 2024](photos/IMG_5043.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05645.jpg)

![Photo of Hack Your Research at IPC 2024](photos/IMG_5018.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05733.jpg)

![Photo of Hack Your Research at IPC 2024](photos/IMG_5028.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05639.jpg)

![Photo of Hack Your Research at IPC 2024](photos/IMG_5052.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05632.jpg)

![Photo of Hack Your Research at IPC 2024](photos/IMG_5035.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05659.jpg)

![Photo of Hack Your Research at IPC 2024](photos/IMG_5024.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05754.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05642.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05629.jpg)

![Photo of Hack Your Research at IPC 2024](photos/DSC05751.jpg)


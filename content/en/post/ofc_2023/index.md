---
date: 2023-02-17T10:58:08-04:00
description: "Event details and list of presenters for OFC 2023."
featured_image: "photos/esOFC23_01557.jpg"
tags: ["OFC", "2023"]
title: "Hack your Research! @ OFC 2023"
author: "Vincent"
draft: false
---

**Event Date:** Sunday, 05 March 2023, 7pm – 9pm PST  

**Event Location:** Room 17, San Diego Convention Center, San Diego, California, USA  

**Link:**
[ofcconference.org/special-events](https://www.ofcconference.org/en-us/home/program-speakers/special-events/#specialEvent-24)

**Organizers:**  
Henrique Buglia, _University College London, United Kingdom_  
Marco Eppenberger, _PsiQuantum, USA_  
Menno van den Hout, _Eindhoven University of Technology, Netherlands_  
Vincent van Vliet, _Eindhoven University of Technology, Netherlands_  

**Advisory Committee:**  
Nicolas Fontaine, _Nokia Bell Labs, USA_  
Binbin Guan, _Microsoft, USA_  
Roland Ryf, _Nokia Bell Labs, USA_  
Jochen Schroeder, _Chalmers University of Technology, Sweden_ 

### Demos:

> **Automated Experimental Dataset Generation in Coherent Optical Communication Systems**  
> by Caio Marciano Santos, Fraunhofer HHI, Germany  
> _We demonstrate sophisticated laboratory automation capable of generating large, diverse and high-quality datasets. The demo will cover the automation process, showcasing automatic setup reconfiguration, monitoring and data storage._

> **Python-based Modelling of Carrier-Phase Recovery for Coherent Optical Transmission Systems**  
> by Denys Zadorozhnyy, Romil Patel and Fernando P. Guiomar, Instituto de Telecomunicações, Universidade de Aveiro, Portugal  
> _This module covers hands-on modelling of coherent optical transmission systems and digital signal processing using Python. The main highlight of this module is to provide an opportunity to learn state-of-the-art carrier-phase noise recovery techniques in modern optical transmission systems. All DSP algorithms will be provided in an open Git repository for the attendees to follow during and/or after the demo._

> **ML-Based Constellation Shaping for Arbitrary Channels**  
> by Beatriz M. Oliveira and Manuel S. Neves, Instituto de Telecomunicações, Universidade de Aveiro, Portugal  
> _Join us for a tutorial on how to implement modular autoencoders in Python using end-to-end learning. You will gain knowledge about ML DSP optimization tools, specifically for the AWGN channel and unamplified coherent systems. Material will be provided to follow along the demo._  
> see [the demo slides](/ofc2023_ml_constellation_shaping_mneves.pdf) and https://github.com/msneves/ConstellationShapingWizard

> **GPU Programming in Python using cupy**  
> by Sjoerd van der Heide, Eindhoven University of Technology, Netherlands  
> _Speed up your code by using your GPU. Many algorithms we use like FFTs and vector/matrix multiplications can be sped up tremendously by unleashing the parallel processing power of your GPU. This demo teaches how to do so in Python._  
> see https://cupy.dev/

> **Petabit/Second Transmission Experiments using SDM Fibers**  
> by Ruben S. Luis, National Institute of Information and Communications Technology (NICT), Japan  
> _During this demonstration we show how we use SDM fibers in our lab to do Pb/s transmission experiments, and we show you how we use advanced lab automation to perform automated measurements._

> **Unleashing the Power of Python in Photonics Design and Simulation**  
> by Eugene Sokolov, Andre Richter, Shi Li, VPI photonics, Germany  
> _Join VPIphotonics experts to see advanced Python-powered techniques used for photonics simulations. Learn how macros can help to automate schematic creation, improve interoperability between tools, estimate transmission systems performance on the example of link budget estimation, and analyze the internal structure of multi-sectional semiconductor lasers._  
> see https://www.vpiphotonics.com/

> **Learn how to Dramatically Reduce your Photonic Design and Hardware Implementation Cycles!**  
> by Daniel Perez-Lopez, Mikel Gutierrez, iPronics, Spain  
> _During this demonstration, we will show how to generate critical photonic functions in minutes using the iPronics SmartLight Processor. Implementations will include interconnects, arbitrary switching, splitters, combiners, independent phase configuration, filtering and equalization._  
> see https://ipronics.com/

> **Swept Wavelength Interferometry for Optical Component and Fiber Characerization**  
> by Lauren Dallachiesa, Nokia Bell Labs, USA  
> _Optical vector network analyzers (OVNAs) are the optical equivalent of electrical VNAs. OVNAs can even be used to measure multiport optical components. We will discuss concepts of swept wavelength interferometry, how to build an OVNA in your lab, and the demo one we made!_  
> see [the demo slides](/ofc2023_swept_wavelength_interferometry_ldallachiesa.pdf)

> **LabExT - the Laboratory Experiment Tool**  
> by Daniel Chelladurai, Institute of Electromagnetic Fields, ETH Zurich, Switzerland
and Benedikt Baeuerle, Polariton Technologies AG, Switzerland  
> _LabExT is a free and open source laboratory automation software for measuring and testing electro-optic integrated circuits. It talks to instruments, runs measurement algorithms, displays graphs and saves the results. Chip-scale automation is possible when used in conjunction with motorized probe stages._  
> see https://github.com/LabExT/LabExT

> **DREAM-ON GYM: A Deep REinforcement Learning FrAMework for Optical Networks**  
> by Hermann Pempelfort, Department of Electronics Engineering, Universidad Técnica Federico Santa María, Valparaiso, Chile and Danilo Borquez-Paredes, Faculty of Engineering and Science, Universidad Adolfo Ibañez, Viña del Mar, Chile and 
Nicolás Jara, Department of Electronics Engineering, Universidad Técnica Federico Santa María, Valparaiso, Chile  
> _An open-source framework for solving resource allocation problems in optical networks using deep reinforcement learning is presented. The software architecture follows OpenAI GYM guidelines, is compatible with Stable Baselines library agents, demonstrating how easily and consistently our tool can solve optical network issues._  
> see [the demo slides](/ofc2023_dream_on_gym_njara.pdf) and https://gitlab.com/IRO-Team/dream-on-gym and https://gitlab.com/IRO-Team/dream-on-gym-app

> **RosenPy: An Open Source Python Framework for CVNNs**  
> by Ariadne Arrais Cruz, Kayol Soares Mayer, Dalton Soares Arantes, Digital Communications Laboratory, Department of Communications, School of Electrical and Computer Engineering, University of Campinas  
> _We demonstrate our recently released framework named RosenPy, a free Python library for complex-valued neural networks (CVNNs). We address some configurations and optimization tools of the RosenPy as well as its application in benchmark datasets._  
> see https://github.com/ariadneac/rosenpy

> **Automating a Reconfigurable Transmission in Real-Time**  
> by Eric Sillekens, Mindaugas Jarmolovičius, University College London, UK  
> _We show how to automate a laboratory to transmit and receive data over a coherent optical system in preparation for real-time signal processing. The automation covers the transmission testbed, including reconfiguring various link parameters, e.g., configuring the recirculating loop and routing the signal over deployed fibre._

> **Gdsfactory, an Open Source python flow for Circuit Design, Verification and Validation**  
> by Joaquin Matres, X The Moonshot Factory (formerly Google X), USA  
> _In this talk we describe the gdsfactory design automation tool. Gdsfactory provides you an end to end workflow that combines layout, verification and validation using an extensible, open source, python driven flow for turning your chip designs into validated products._  
> see https://gdsfactory.github.io/gdsfactory/

> **Photonic Design Automation using KLayout Integration in Ansys Lumerical**  
> by Milad Mahpeykar, Ansys, USA  
> _In this demo, we will present a photonic design ecosystem built around KLayout, an open-source layout design tool, using Ansys Lumerical photonic simulation tools. This will enable designers to use KLayout in both component and circuit-level design flows for a fully layout-aware design process._  
> see [https://optics.ansys.com](https://optics.ansys.com/hc/en-us/articles/4411236218387-KLayout-Interoperability-Overview-)

> **Commplax: A opensource Python DSP Framework for Optical Communications, Accelerated in CPU/GPU, powered by JAX**  
> by Remi Qirui Fan and Alan Pak Tao Lau, The Hong Kong Polytechnic University  
> _Commplax is a JAX-based DSP dev platform for optical communications that is ultra-fast to run on both CPU and GPU. It interfaces itself as NeuralNet layers to interact with other ML frameworks. JAX is a superstar framework for scientific computing with familiar Numpy interfaces._  
> see https://github.com/remifan/commplax

### Video Demos:

> **Automated Experimental Dataset Generation in Coherent Optical Communication Systems**  
> by Caio Marciano Santos, Fraunhofer HHI, Germany  
> _We demonstrate sophisticated laboratory automation capable of generating large, diverse and high-quality datasets. The video exemplifies the beneficial aspect of laboratory automation for long measurement campaigns where multiple system parameters are changed within each measurement._  
> see https://www.youtube.com/watch?v=TlB4CHPEzp0

> **Real-Time Demonstration of Optical Wireless Technologies**  
> by Marco A. Fernandes and Pedro A. Loureiro, Instituto de Telecomunicações, Universidade de Aveiro, Portugal  
> _We demonstrate real-time optical wireless communications in a practical indoor video streaming scenario. Complementary transmission technologies are demonstrated, including visible light communications (VLC) when room illumination is being used, and infrared free-space optics (FSO) when illumination is dimmed or shut off._  
> see https://www.youtube.com/watch?v=I_d78aA43pE

### Lightning Talks:

> **Object-Oriented Programming in Matlab for Laboratory Automation**  
> by Caio Marciano Santos, Fraunhofer HHI, Germany  
> _A brief description on using object-oriented programming for instrument control in Matlab, simplifying remote operation functions of laboratory equipment._  
> see [the talk slides](/ofc2023_lt1_caio_oop_lab_automation.pdf)

> **Tmux for Lab Control (and more) in Linux**  
> by Domaniç Lavery, Infinera, Canada  
> _Tmux (Terminal multiplexer) is a great multi-tasking tool for linux terminal users. If you ever used ‘screen’, you should be using Tmux!_

> **WebPlotDigitizer: Retrieving Data from Pictures of Plots**  
> by Domaniç Lavery, Infinera, Canada  
> _For when you need data from a plot graphic (and you can’t be bothered to load Python!)_  
> see https://apps.automeris.io/wpd/

> **Can chatGPT be used to Power up your Research Code?**  
> by Maria Ionescu, Infinera, Sweden  
> _ChatGPT can generate high-quality and relevant code snippets based on natural language descriptions of the desired implementation. How could chatGPT be explored in saving time and improving accuracy, particularly for tasks involving modeling, DSP implementation, as well as lab work?_

### Photos:

Partially from [OFC Photo Gallery](https://www.ofcconference.org/en-us/home/about/photo-gallery-2023/awards-ceremony-(5)/)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01557.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01533.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01528.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01522.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01516.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01579.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01511.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01541.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01557.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01590.jpg)

![Photo of Hack Your Research at OFC 2023](photos/esOFC23_01552.jpg)

![Photo of Hack Your Research at OFC 2023](photos/IMG_7555.jpg)